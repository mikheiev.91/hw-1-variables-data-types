//  1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

let myName;
let admin;

myName = 'Maksym';
admin = myName;
console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 2;

function daysToSeconds(numberOfDays) {
	let secondsInMinute = 60;
	let minutesInHour = 60;
	let hoursInDay = 24;
	let secondsInDays = numberOfDays * (hoursInDay * minutesInHour * secondsInMinute);
	return secondsInDays;
}

console.log(daysToSeconds(days));

// 3. Запитайте у користувача якесь значення і виведіть його в консоль.

let favouriteDrink = prompt('What is your favourite drink?');

console.log(favouriteDrink);